import 'dart:io';
import 'fotos.dart';
import 'login.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:topicos_camara/screens/login.dart';
import 'package:topicos_camara/widgets/avatarField.dart';

class Register extends StatefulWidget {
  final Map<String,String> names;
  Register(this.names);
  @override
  _RegisterState createState() => _RegisterState(this.names, names.keys.toList()[0]);
}

class _RegisterState extends State<Register> {
  final Map<String,String> names;
  String loc;
  _RegisterState(this.names, this.loc);
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  var fname;
  var lname;
  var address;
  var phone;
  var photo;
  var email;
  var password;
  var vpassword;
  var CI;
  var fecha = TextEditingController();
  var dat;
  String ra = "Empleado";
  List<String> rol=["Empleado", "Empleador"];
  List<DropdownMenuItem<String>> cargar(){
    List<DropdownMenuItem<String>> ddi=[];
    names.forEach((key, value) {
      ddi.add(
        DropdownMenuItem<String>(
            value: key.toString(),
            child: Text(value),
        )
      );
    });
    return ddi;
  }

  String dateTime;

  DateTime selectedDate = DateTime(DateTime.now().year-18, DateTime.now().month, DateTime.now().day);

  TextEditingController _dateController = TextEditingController();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        cancelText: "Cancelar",
        confirmText: "OK",
        context: context,
        initialDate: selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(DateTime.now().year-18, DateTime.now().month, DateTime.now().day));
    if (picked != null)
      setState(() {
        selectedDate = picked;
        var dia = selectedDate.day.toString();
        if(selectedDate.day < 10){
          dia = '0'+dia;
        }
        var mes = selectedDate.month.toString();
        if(selectedDate.month < 10){
          mes = '0'+mes;
        }
        _dateController.text = dia+'/'+mes+'/'+selectedDate.year.toString();
      });
  }

  @override
  void initState() {
    var dia = selectedDate.day.toString();
    if(selectedDate.day < 10){
      dia = '0'+dia;
    }
    var mes = selectedDate.month.toString();
    if(selectedDate.month < 10){
      mes = '0'+mes;
    }
    _dateController.text = dia+'/'+mes+'/'+selectedDate.year.toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registrarse'),
      ),
      body: Container(
        color: Colors.cyan[400],
        child: SingleChildScrollView(
          child:Stack(
            children: <Widget>[
              Positioned(
                child: Padding(
                  padding: EdgeInsets.only(top:8.0, right:8.0, left:8.0, bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Card(
                        elevation: 0,
                        color: Colors.white,
                        margin: EdgeInsets.only(left: 20, right: 20),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: AvatarFormField(
                                    context: context,
                                    autovalidate: false,
                                    validator: (photoPath) {
                                      if (photoPath == null) {
                                        return 'Ingrese una fotografía suya.';
                                      }
                                      photo = photoPath;
                                      return null;
                                    },
                                  ),
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.number,
                                  maxLength: 8,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.perm_identity,
                                      color: Colors.grey,
                                    ),
                                    hintText: "CI",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (CIValue) {
                                    if (CIValue.isEmpty) {
                                      return 'Ingrese su CI.';
                                    }
                                    if (CIValue.length < 6) {
                                      return 'Introduzca un CI con al menos 6 digitos';
                                    }
                                    CI = CIValue;
                                    return null;
                                  },
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.face,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Nombre(s)",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (firstname) {
                                    if (firstname.isEmpty) {
                                      return 'Ingrese su(s) nombre(s).';
                                    }
                                    fname = firstname;
                                    return null;
                                  },
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.family_restroom,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Apellido",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (lastname) {
                                    if (lastname.isEmpty) {
                                      return 'Ingrese su apellido.';
                                    }
                                    lname = lastname;
                                    return null;
                                  },
                                ),
                                Column(
                                  children: <Widget>[
                                    Text(
                                      'Fecha de Nacimiento',
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic,
                                          fontWeight: FontWeight.w600,
                                          letterSpacing: 0.5),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        _selectDate(context);
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(color: Colors.grey[200]),
                                        child: TextFormField(
                                          style: TextStyle(fontSize: 15),
                                          textAlign: TextAlign.center,
                                          enabled: false,
                                          keyboardType: TextInputType.text,
                                          controller: _dateController,
                                          decoration: InputDecoration(
                                              disabledBorder:
                                              UnderlineInputBorder(borderSide: BorderSide.none),
                                              // labelText: 'Time',
                                              contentPadding: EdgeInsets.only(top: 0.0)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.pin_drop,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Dirección",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (addressstring) {
                                    if (addressstring.isEmpty) {
                                      return 'Ingrese su dirección.';
                                    }
                                    if(addressstring.length<5){
                                      return 'Ingrese una direccion valida.';
                                    }
                                    address = addressstring;
                                    return null;
                                  },
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.number,
                                  maxLength: 8,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.phone,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Teléfono",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (phonenumber) {
                                    if (phonenumber.isEmpty) {
                                      return 'Ingrese un número de teléfono.';
                                    }
                                    if(phonenumber.length<6){
                                      return 'Ingrese un número de teléfono valido.';
                                    }
                                    phone = phonenumber;
                                    return null;
                                  },
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.email,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Email",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (emailValue) {
                                    if (emailValue.isEmpty) {
                                      return 'Ingrese su correo electrónico';
                                    }
                                    if (RegExp(
                                        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                        .hasMatch(emailValue)) {
                                      email = emailValue;
                                      return null;
                                    }
                                    else {
                                      return "Ingrese un correo electrónico válido.";
                                    }
                                  },
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.vpn_key,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Password",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (passwordValue) {
                                    if (passwordValue.isEmpty) {
                                      return 'Ingrese una contraseña válida.';
                                    }
                                    var seguro;
                                    if (passwordValue.length >= 7) {
                                      var mayuscula = false;
                                      var minuscula = false;
                                      var numero = false;
                                      var caracter_raro = false;

                                      for (var i = 0; i <
                                          passwordValue.length; i++) {
                                        if (passwordValue.toString().codeUnitAt(
                                            i) >= 65 &&
                                            passwordValue.toString().codeUnitAt(
                                                i) <= 90) {
                                          mayuscula = true;
                                        }
                                        else
                                        if (passwordValue.toString().codeUnitAt(
                                            i) >= 97 &&
                                            passwordValue.toString().codeUnitAt(
                                                i) <= 122) {
                                          minuscula = true;
                                        }
                                        else
                                        if (passwordValue.toString().codeUnitAt(
                                            i) >= 48 &&
                                            passwordValue.toString().codeUnitAt(
                                                i) <= 57) {
                                          numero = true;
                                        }
                                        else {
                                          caracter_raro = true;
                                        }
                                      }
                                      if (mayuscula && minuscula &&
                                          caracter_raro && numero) {
                                        password = passwordValue;
                                        return seguro;
                                      }
                                      else {
                                        if (!mayuscula) {
                                          return "La contraseña requiere al menos una letra mayuscula";
                                        }
                                        if (!minuscula) {
                                          return "La contraseña requiere al menos una letra minuscula";
                                        }
                                        if (!caracter_raro) {
                                          return "La contraseña requiere al menos un caracter especial";
                                        }
                                        if (!numero) {
                                          return "La contraseña requiere al menos un numero";
                                        }
                                      }
                                    }
                                    else {
                                      return "La contraseña es muy corta";
                                    }
                                  },
                                ),
                                TextFormField(
                                  style: TextStyle(color: Color(0xFF000000)),
                                  cursorColor: Color(0xFF9b9b9b),
                                  keyboardType: TextInputType.text,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      Icons.vpn_key,
                                      color: Colors.grey,
                                    ),
                                    hintText: "Verificar Password",
                                    hintStyle: TextStyle(
                                        color: Color(0xFF9b9b9b),
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal),
                                  ),
                                  validator: (passwordValue) {
                                    if (passwordValue.isEmpty) {
                                      return 'Verifique la contraseña.';
                                    }
                                    if (passwordValue != password) {
                                      return 'Las contraseñas no coinciden.';
                                    }
                                    vpassword = passwordValue;
                                    return null;
                                  },
                                ),
                                DropdownButton<String>(
                                  value: loc,
                                  icon: Icon(Icons.arrow_downward),
                                  style: TextStyle(color: Color(0xFF000000)),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      loc = newValue;
                                    });
                                  },
                                  items: cargar(),
                                ),
                                DropdownButton<String>(
                                  value: ra,
                                  icon: Icon(Icons.arrow_downward),
                                  style: TextStyle(color: Color(0xFF000000)),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      ra = newValue;
                                    });
                                  },
                                  items: rol.map<DropdownMenuItem<String>>((
                                      String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: FlatButton(
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                          top: 8,
                                          bottom: 8,
                                          left: 10,
                                          right: 10),
                                      child: Text(
                                        _isLoading
                                            ? 'Registrando usuario...'
                                            : ra=="Empleado"
                                              ?"Siguiente"
                                              :"Registrar",
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15.0,
                                          decoration: TextDecoration.none,
                                          fontWeight: FontWeight.normal,
                                        ),
                                      ),
                                    ),
                                    color: Colors.blue[400],
                                    disabledColor: Colors.grey,
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                        new BorderRadius.circular(20.0)),
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        if(ra=="Empleado"){
                                          var ap=lname.toString().split(" ");
                                          var ap2=ap.length>1 ?ap[1] :'';
                                          var datos = '{'+
                                            '"nombre": "'+fname.toString()+'",'+
                                            '"AP": "'+ap[0]+'",'+
                                            '"AM": "'+ap2+'",'+
                                            '"ci": "'+CI.toString()+'",'+
                                            '"dir": "'+address.toString()+'",'+
                                            '"loc": "'+loc.toString()+'",'+
                                            '"email": "'+email.toString()+'",'+
                                            '"pass": "'+password.toString()+'",'+
                                            '"FN": "'+selectedDate.year.toString()+'-'+selectedDate.month.toString()+'-'+selectedDate.day.toString()+'",'+
                                            '"FR": "'+DateFormat('yyyy-MM-dd').format(DateTime.now())+'",'+
                                            '"telefono": "'+phone.toString()+'",'+
                                            '"opcion": "RegistrarUsuario",'
                                            '"empleado": "si"'
                                          '}';
                                          Navigator.push(
                                              context,
                                              new MaterialPageRoute(
                                                  builder: (context) => Fotos(jsonDecode(datos), photo)));
                                        }
                                        else{
                                          _register();
                                        }
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                new MaterialPageRoute(
                                    builder: (context) => Login()));
                          },
                          child: Text(
                            'Iniciar sesión',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15.0,
                              decoration: TextDecoration.none,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAlertDialog() {
    return AlertDialog(
      title: Text('Registro exitoso'),
      content:
      Text("Su cuenta fue registrada exitosamente, espere la confirmacion de activacion de cuenta"),
      actions: [
        FlatButton(
            child: Text("Aceptar"),
            textColor: Colors.blue,
            onPressed: () {
              Navigator.popUntil(context, (Route<dynamic> route) => route.isFirst);
            }),
      ],
    );
  }

  void _register() async {
    setState(() {
      _isLoading = true;
    });
    var ap=lname.toString().split(" ");
    var request = http.MultipartRequest('POST', Uri.parse('http://192.168.1.160/topicos/controladores/APIUsuarioController'));
    request.fields.addAll({
      'nombre': fname.toString(),
      'AP': ap[0],
      'AM': ap.length>1 ?ap[1] :'',
      'ci': CI.toString(),
      'dir': address.toString(),
      'loc': loc.toString(),
      'email': email.toString(),
      'pass': password.toString(),
      'FN': selectedDate.year.toString()+'-'+selectedDate.month.toString()+'-'+selectedDate.day.toString(),
      'FR': DateFormat('yyyy-MM-dd').format(DateTime.now()),
      'telefono': phone.toString(),
      'opcion': 'RegistrarUsuario',
      'empleado': 'no'
    });
    request.files.add(await http.MultipartFile.fromPath('FP', photo));

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      showDialog(
        context: context,
        builder: (_) => _buildAlertDialog(),
      );
    }
    else {
      print("no");
    }

    setState(() {
      _isLoading = false;
    });
  }
}
