import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MenuLateral extends StatefulWidget {
  MenuLateral({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MenuLateralState createState() => MenuLateralState();
}


class MenuLateralState extends State<MenuLateral>{
  var Nombre="";
  var Rol = "";
  void nombre() async{
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      Nombre = (prefs.getString('nombre') ?? "");
      Rol = (prefs.getString('rol') ?? "");
    });
  }
  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    nombre();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: Text(Nombre, style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 15),),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: new AssetImage('images/logo.png'),
                    fit: BoxFit.cover
                )
            ),
          ),
          new ListTile(
            title: Text(Rol=="Empleado"?"Inicio":"Trabajadores"),
            onTap: (){},
          ),
          new ListTile(
            title: Text(Rol=="Empleado"?"Mis ofertas":"Mis solicitudes"),
            onTap: (){},
          ),
          new ListTile(
            title: Text("Cerrar sesion"),
            onTap: ()async{
              final prefs = await SharedPreferences.getInstance();
              prefs.remove('ID');
              prefs.remove('nombre');
              prefs.remove('rol');
              Navigator.pop(context);
            },
          )
        ],
      ) ,
    );
  }
}