import 'dart:io';
import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import '../widgets/selects.dart';
import 'package:date_format/date_format.dart';

class Oficios extends StatefulWidget {
  var datos;
  var FA;
  var FCI;
  var FS;
  var oficios;
  var FP;
  Oficios(this.datos, this.FA, this.FCI, this.FS, this.FP, this.oficios);
  @override
  _OficiosState createState() => _OficiosState(this.datos, this.FA, this.FCI, this.FS, this.FP, this.oficios, this.oficios['padres'][0]['ID']);
}

class _OficiosState extends State<Oficios> {
  var datos;
  var FA;
  var FCI;
  var FS;
  var oficios;
  var padre;
  var subcategoria;
  var oficio;
  var FP;
  Map<int,TimeOfDay> TimesInit={};
  Map<int,TextEditingController> TimesInitC={};
  Map<int,TimeOfDay> TimesFin={};
  Map<int,TextEditingController> TimesFinC={};
  Map<int,Widget> WO={};
  _OficiosState(this.datos, this.FA, this.FCI, this.FS, this.FP, this.oficios, this.padre);
  final _formKey = GlobalKey<FormState>();
  
  Future<Null> _selectTime(BuildContext context,id, time, control) async {
    final TimeOfDay picked = await showTimePicker(
      cancelText: "Cancelar",
      confirmText: "OK",
      context: context,
      initialTime: time,
    );
    if (picked != null)
      setState(() {
        time = picked;
        control.text = formatDate(
            DateTime(2019, 08, 1, time.hour, time.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  void agregar(){
    var nombre;
    var id = int.parse(oficio);
    for(var fila in oficios['oficios']){
      if(fila['ID'].toString() == oficio.toString()){
        nombre = fila['nombre'];
      }
    }

    TimesInitC[id] = new TextEditingController();
    TimesFinC[id] = new TextEditingController();
    TimesInit[id] = TimeOfDay(hour: 00, minute: 00);
    TimesFin[id] = TimeOfDay(hour: 01, minute: 00);
    TimesInitC[id].text=formatDate(
        DateTime(2019, 08, 1, TimesInit[id].hour, TimesInit[id].minute),
        [hh, ':', nn, " ", am]).toString();
    TimesFinC[id].text=formatDate(
        DateTime(2019, 08, 1, TimesFin[id].hour, TimesFin[id].minute),
        [hh, ':', nn, " ", am]).toString();

    Widget tarjeta = Column(
      children: <Widget>[
        Text(
          nombre,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.w600,
              letterSpacing: 0.5),
        ),
        Row(
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  'Hora de Inicio',
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5),
                ),
                InkWell(
                  onTap: () {
                    _selectTime(context,id,TimesInit[id],TimesInitC[id]);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    child: TextFormField(
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                      enabled: false,
                      keyboardType: TextInputType.text,
                      controller: TimesInitC[id],
                      decoration: InputDecoration(
                          disabledBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                          // labelText: 'Time',
                          contentPadding: EdgeInsets.all(5)),
                    ),
                  ),
                ),
              ],
            ),
            Column(
              children: <Widget>[
                Text(
                  'Hora Fin',
                  style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0.5),
                ),
                InkWell(
                  onTap: () {
                    _selectTime(context,id,TimesFin[id],TimesFinC[id]);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    child: TextFormField(
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                      enabled: false,
                      keyboardType: TextInputType.text,
                      controller: TimesFinC[id],
                      decoration: InputDecoration(
                          disabledBorder:
                          UnderlineInputBorder(borderSide: BorderSide.none),
                          // labelText: 'Time',
                          contentPadding: EdgeInsets.all(5)),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: FlatButton(
            child: Padding(
              padding: EdgeInsets.only(
                  top: 8,
                  bottom: 8,
                  left: 10,
                  right: 10),
              child: Text(
                "Eliminar",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                  decoration: TextDecoration.none,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
            color: Colors.blue[400],
            disabledColor: Colors.grey,
            shape: new RoundedRectangleBorder(
                borderRadius:
                new BorderRadius.circular(20.0)),
            onPressed: () {
              agregar();
            },
          ),
        ),
      ],
    );
    WO[id] = tarjeta;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Oficios'),
      ),
      body: MaterialApp(
            home: Select(this.datos, this.FA, this.FCI, this.FS, this.FP, this.oficios),//DropdownTestView(),//CheckAuth(),
            debugShowCheckedModeBanner: false,
          )
    );
  }
}
