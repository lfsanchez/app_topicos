import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:topicos_camara/screens/oficios.dart';
import 'package:topicos_camara/widgets/avatarField.dart';

class Fotos extends StatefulWidget {
  var datos;
  var FP;
  Fotos(this.datos, this.FP);
  @override
  _FotosState createState() => _FotosState(this.datos, this.FP);
}

class _FotosState extends State<Fotos> {
  var datos;
  var FP;
  _FotosState(this.datos, this.FP);
  final _formKey = GlobalKey<FormState>();
  var antecedentes;
  var ci;
  var selfie;
  var oficios;
  void listaoficios() async {
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var request = http.Request('POST', Uri.parse('http://192.168.1.160/topicos/controladores/APIDMController'));
    request.bodyFields = {
      'opcion': 'oficios'
    };
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      oficios = jsonDecode(await response.stream.bytesToString());
    }
    else {
      print(response.reasonPhrase);
    }
  }
  @override
  Widget build(BuildContext context) {
    listaoficios();
    return Scaffold(
      appBar: AppBar(
        title: Text('Fotografias de validacion'),
      ),
      body: Container(
        color: Colors.cyan[400],
        child: ListView(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Positioned(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Card(
                          elevation: 0,
                          color: Colors.white,
                          margin: EdgeInsets.only(left: 20, right: 20),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Form(
                              key: _formKey,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text("FOTO CI",textAlign: TextAlign.center,),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: AvatarFormField(
                                      context: context,
                                      autovalidate: false,
                                      validator: (photoPath) {
                                        if (photoPath == null) {
                                          return 'Ingrese una fotografía de su CI.';
                                        }
                                        ci = photoPath;
                                        return null;
                                      },
                                    ),
                                  ),
                                  Text("FOTO CERTIFICADO DE ANTECEDENTES PENALES",textAlign: TextAlign.center,),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: AvatarFormField(
                                      context: context,
                                      autovalidate: false,
                                      validator: (photoPath) {
                                        if (photoPath == null) {
                                          return 'Ingrese una fotografía de su certificado de antecedentes penales.';
                                        }
                                        antecedentes = photoPath;
                                        return null;
                                      },
                                    ),
                                  ),
                                  Text("FOTO SELFIE SOSTENIENDO CI", textAlign: TextAlign.center,),
                                  Padding(
                                    padding: const EdgeInsets.all(15.0),
                                    child: AvatarFormField(
                                      context: context,
                                      autovalidate: false,
                                      validator: (photoPath) {
                                        if (photoPath == null) {
                                          return 'Ingrese una fotografía suya sosteniendo su CI.';
                                        }
                                        selfie = photoPath;
                                        return null;
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: FlatButton(
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            top: 8,
                                            bottom: 8,
                                            left: 10,
                                            right: 10),
                                        child: Text(
                                          "siguiente ",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15.0,
                                            decoration: TextDecoration.none,
                                            fontWeight: FontWeight.normal,
                                          ),
                                        ),
                                      ),
                                      color: Colors.blue[400],
                                      disabledColor: Colors.grey,
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                          new BorderRadius.circular(20.0)),
                                      onPressed: () {
                                        if (_formKey.currentState.validate()) {
                                          Navigator.push(
                                              context,
                                              new MaterialPageRoute(
                                                  builder: (context) => Oficios(datos, antecedentes, ci, selfie, FP, oficios)));
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
