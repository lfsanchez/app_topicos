import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:topicos_camara/network_utils/api.dart';
import 'package:topicos_camara/screens/home.dart';
import 'package:topicos_camara/screens/register.dart';
import 'package:http/http.dart' as http;
import 'home.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  Map<String, String> names={};
  bool _isLoading = false;
  String ra = "Empleado";
  List<String> rol=["Empleado", "Empleador"];
  var email;
  var password;
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  void sesion() async {
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var request = http.Request('POST', Uri.parse('http://192.168.1.160/topicos/controladores/APIDMController'));
    request.bodyFields = {
      'opcion': 'ciudades'
    };
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      List<dynamic> respuesta = jsonDecode(await response.stream.bytesToString());
      for(var fila in respuesta){
        if(names.length>0){
          if(!names.containsKey(fila['ID']))
            names[fila['ID']]=fila['nombre'];
        }
        else{
          names[fila['ID']]=fila['nombre'];
        }
      }
    }
    else {
      print(response.reasonPhrase);
    }
  }
  @override
  Widget build(BuildContext context) {
    sesion();
    // Build a Form widget using the _formKey created above.
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      key: _scaffoldKey,
      body: Container(
        color: Colors.blue[300],
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Card(
                      elevation: 0,
                      color: Colors.white,
                      margin: EdgeInsets.only(left: 10, right: 10),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextFormField(
                                style: TextStyle(color: Color(0xFF000000)),
                                cursorColor: Color(0xFF9b9b9b),
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.email,
                                    color: Colors.black,
                                  ),
                                  hintText: "Email",
                                  hintStyle: TextStyle(
                                      color: Color(0xFF9b9b9b),
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal),
                                ),
                                validator: (emailValue) {
                                  if (emailValue.isEmpty) {
                                    return 'Ingresar correo electrónico.';
                                  }
                                  email = emailValue;
                                  return null;
                                },
                              ),
                              TextFormField(
                                style: TextStyle(color: Color(0xFF000000)),
                                cursorColor: Color(0xFF9b9b9b),
                                keyboardType: TextInputType.text,
                                obscureText: true,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.vpn_key,
                                    color: Colors.black,
                                  ),
                                  hintText: "Password",
                                  hintStyle: TextStyle(
                                      color: Color(0xFF9b9b9b),
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal),
                                ),
                                validator: (passwordValue) {
                                  if (passwordValue.isEmpty) {
                                    return 'Ingresar clave.';
                                  }
                                  password = passwordValue;
                                  return null;
                                },
                              ),
                              DropdownButton<String>(
                                value: ra,
                                icon: Icon(Icons.arrow_downward),
                                style: TextStyle(color: Color(0xFF000000)),
                                onChanged: (String newValue) {
                                  setState(() {
                                    ra = newValue;
                                  });
                                },
                                items: rol.map<DropdownMenuItem<String>>((
                                    String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: FlatButton(
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        top: 8, bottom: 8, left: 10, right: 10),
                                    child: Text(
                                      _isLoading
                                          ? 'Iniciando Sesión...'
                                          : 'Iniciar Sesión',
                                      textDirection: TextDirection.ltr,
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15.0,
                                        decoration: TextDecoration.none,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  color: Colors.blue[400],
                                  disabledColor: Colors.grey,
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(20.0)),
                                  onPressed: () {
                                    if (_formKey.currentState.validate()) {
                                      _login();
                                    }
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 15),
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => Register(names)));
                        },
                        child: Text(
                          'Crear una cuenta',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            decoration: TextDecoration.none,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _login() async {
    setState(() {
      _isLoading = true;
    });
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded'
    };
    var emp="si";
    if(ra != "Empleado"){
      emp="no";
    }
    var request = http.Request('POST', Uri.parse('http://192.168.1.160/topicos/controladores/APIUsuarioController'));
    request.bodyFields = {
      'email': email,
      'pass': password,
      'opcion': 'Login',
      'empleado': emp
    };
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      var respuesta = await response.stream.bytesToString();
      if(respuesta=="password"){
        showDialog(
          context: context,
          builder: (_) => _buildAlertDialog("Contraseña erronea"),
        );
      }
      else{
        if(respuesta=="usuario"){
          showDialog(
            context: context,
            builder: (_) => _buildAlertDialog("No se encuentra al usuario"),
          );
        }
        else{
          var valor = jsonDecode(respuesta);
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('CI', valor["ID"]);
          prefs.setString('Nombre', valor["nombre"]);
          prefs.setString('rol', ra);
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => Home()));
        }
      }
    }
    else {
      print(response.reasonPhrase);
    }

    setState(() {
      _isLoading = false;
    });
  }

  Widget _buildAlertDialog(mensaje) {
    return AlertDialog(
      title: Text('Alerta'),
      content:
      Text(mensaje),
      actions: [
        FlatButton(
            child: Text("Aceptar"),
            textColor: Colors.blue,
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ],
    );
  }

  _showMsg(msg) {
    final snackBar = SnackBar(
      content: Text(msg),
      action: SnackBarAction(
        label: 'Close',
        onPressed: () {
          // Some code to undo the change!
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
