import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:topicos_camara/screens/login.dart';
import 'package:topicos_camara/network_utils/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Menu.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String ID;
  String nombre;
  String rol;
  var oficios;
  var datos;

  @override
  void initState() {
    super.initState();
    _loadUserData();
    loadWidget();
  }

  Widget loadWidget(){
    if(rol=="Empleador"){
      return Container(

      );
    }
    else{
      return Container(
        child:Column(
          children: [
            Text("hola"),
          ],
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('YoTrabajo'),
      ),
      drawer: MenuLateral(),
      body: datos,
    );
  }

  _loadUserData() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var user = localStorage.getString('ID');

    if (user != null) {
      setState(() {
        ID=user;
        nombre=localStorage.getString('nombre');
        rol=localStorage.getString('rol');
      });
    }
  }

  void logout() async {
    var res = await Network().getData('/logout');
    var body = json.decode(res.body);
    if (body['success']) {
      SharedPreferences localStorage = await SharedPreferences.getInstance();
      localStorage.remove('user');
      localStorage.remove('token');
      Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
    }
  }
}
