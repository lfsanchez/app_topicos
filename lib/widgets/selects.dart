import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';
import '../main.dart';

class Select extends StatefulWidget {
  final ofis;
  final datos;
  final FA;
  final FCI;
  final FS;
  final FP;
  const Select(this.datos, this.FA, this.FCI, this.FS, this.FP, this.ofis,{Key key}) : super(key: key);
  Map<String,String> padres(){
    Map<String,String> padres={};
    for(var valor in ofis['padres']) {
      padres[valor['ID']]=valor['nombre'];
    }
    return padres;
  }
  Map<String,Map<String,String>> suboficios(){
    Map<String,Map<String,String>> padres={};
    for(var valor in ofis['padres']) {
      Map<String,String> subcat={};
      for(var sub in ofis['subcategorias']) {
        if(sub["padre"] == valor["ID"]) {
          subcat[sub["ID"]]=sub["nombre"];
        }
      }
      if(subcat.length>0) {
        padres[valor['ID']]=subcat;
      }
      else{
        subcat['-1']="No se encuentran subcategorias asociadas";
        padres[valor['ID']]=subcat;
      }
    }
    return padres;
  }
  Map<String,Map<String,String>> oficios(){
    Map<String,Map<String,String>> padres={};
    for(var valor in ofis['subcategorias']) {
      Map<String,String> subcat={};
      for(var sub in ofis['oficios']) {
        if(sub["padre"] == valor["ID"]) {
          subcat[sub["ID"]]=sub["nombre"];
        }
      }
      if(subcat.length>0) {
        padres[valor['ID']]=subcat;
      }
      else{
        subcat['-1']="No se encuentran oficios asociados";
        padres[valor['ID']]=subcat;
      }
    }
    Map<String,String>s={};
    s["-1"]="No se encuentran oficios asociados";
    padres['-1']=s;
    return padres;
  }
  String pp(){
    return padres().keys.toList()[0];
  }
  String ps(){
    String padre=padres().keys.toList()[0];
    Map<String,Map<String,String>> sub = suboficios();
    return sub[padre].keys.toList()[0];
  }
  String po(){
    String sub = ps();
    Map<String,Map<String,String>> of = oficios();
    return of[sub].keys.toList()[0];
  }
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState(this.datos, this.FA, this.FCI, this.FS, this.FP, this.ofis,padres(), suboficios(), oficios(), pp(), ps(), po());
}


class _MyStatefulWidgetState extends State<Select> {
  final datos;
  final ofis;
  var FA;
  var FCI;
  var FS;
  var FP;
  final Map<String,Map<String,String>> oficios;
  final Map<String,Map<String,String>> suboficios;
  final Map<String,String> padres;
  String padre;
  String sub;
  String of;
  _MyStatefulWidgetState(this.datos, this.FA, this.FCI, this.FS, this.FP, this.ofis, this.padres, this.suboficios, this.oficios, this.padre, this.sub, this.of);

  Map<String,TimeOfDay> TimesInit={};
  Map<String,TextEditingController> TimesInitC={};
  Map<String,TimeOfDay> TimesFin={};
  Map<String,TextEditingController> TimesFinC={};
  Map<String,Widget> widgetList = {};

  void _register() async {
    String oficios = "{";
    for(var index in widgetList.keys.toList()){
      String HIH=TimesInit[index].hour<10 ?"0"+TimesInit[index].hour.toString():TimesInit[index].hour.toString();
      String HIM=TimesInit[index].minute<10 ?"0"+TimesInit[index].minute.toString():TimesInit[index].minute.toString();
      String HFH=TimesFin[index].hour<10 ?"0"+TimesFin[index].hour.toString():TimesFin[index].hour.toString();
      String HFM=TimesFin[index].minute<10 ?"0"+TimesFin[index].minute.toString():TimesFin[index].minute.toString();
      oficios +='"'+index.toString()+'":'+'{'+
        '"ID": "'+index+'",'+
        '"registro": "'+DateFormat('yyyy-MM-dd').format(DateTime.now())+'",'+
        '"HI": "'+HIH+':'+HIM+':00",'+
        '"HF": "'+HFH+':'+HFM+':00"'+
      "}";
    };
    oficios+="}";
    this.datos["oficios"] = oficios.toString();
    var request = http.MultipartRequest('POST', Uri.parse('http://192.168.1.160/topicos/controladores/APIUsuarioController'));
    request.fields.addAll(new Map<String, String>.from(this.datos));
    request.files.add(await http.MultipartFile.fromPath('FP', this.FP));
    request.files.add(await http.MultipartFile.fromPath('FA', this.FA));
    request.files.add(await http.MultipartFile.fromPath('FC', this.FCI));
    request.files.add(await http.MultipartFile.fromPath('FRC', this.FS));

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      showDialog(
        context: context,
        builder: (_) => _buildAlertDialog(),
      );
    }
    else {
      print("no");
    }
  }

  Widget _buildAlertDialog() {
    return AlertDialog(
      title: Text('Registro exitoso'),
      content:
      Text("Su cuenta fue registrada exitosamente, espere la confirmacion de activacion de cuenta"),
      actions: [
        FlatButton(
            child: Text("Aceptar"),
            textColor: Colors.blue,
            onPressed: () {
              Navigator.of(context).pop();
            }),
      ],
    );
  }

  Future<Null> _selectTime(BuildContext context,id, time, control, init) async {
    final TimeOfDay picked = await showTimePicker(
      cancelText: "Cancelar",
      confirmText: "OK",
      context: context,
      initialTime: time,
    );

    if (picked != null){
      time = picked;
      if(init){
        TimesInit[id]=picked;
      }
      else{
        TimesFin[id]=picked;
      }
    }
    control.text = formatDate(
        DateTime(2019, 08, 1, time.hour, time.minute),
        [hh, ':', nn, " ", am]).toString();
    if(init && TimesFin[id].hour<picked.hour+1){
      TimesFin[id] = TimeOfDay(hour: picked.hour+1, minute: picked.minute);
      TimesFinC[id].text = formatDate(
          DateTime(2019, 08, 1, picked.hour+1, picked.minute),
          [hh, ':', nn, " ", am]).toString();
    }
    else{
      if(!init && TimesInit[id].hour>picked.hour-1) {
        TimesInit[id] = TimeOfDay(hour: picked.hour - 1, minute: picked.minute);
        TimesInitC[id].text = formatDate(
            DateTime(2019, 08, 1, picked.hour - 1, picked.minute),
            [hh, ':', nn, " ", am]).toString();
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  List<DropdownMenuItem<String>> itemsToList(List<String> items){
    return items.map<DropdownMenuItem<String>>((String value) {
      return DropdownMenuItem<String>(
        value: value,
        child: Text(value),
      );
    }).toList();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Center( child: dropdown1()),
          Center( child: dropdown2()),
          Center( child: dropdown3()),
          Center( child: botonAumentarWidget(context) ),
          Center(
            child:Column(
              children: [
                Center(child: listOfWidgets()),
              ],
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(10.0),
                child: FlatButton(
                  child: Padding(
                    padding: EdgeInsets.only(
                    top: 8,
                    bottom: 8,
                    left: 10,
                    right: 10),
                  child: Text(
                    "Registrar",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15.0,
                      decoration: TextDecoration.none,
                      fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  color: Colors.blue[400],
                  disabledColor: Colors.grey,
                  shape: new RoundedRectangleBorder(
                    borderRadius:
                    new BorderRadius.circular(20.0)),
                  onPressed: () {
                    if (widgetList.length>0) {
                    _register();
                    }
                  },
                ),
              ),
          ),
        ],
      )
    );
  }

  Widget listOfWidgets() {
    if(widgetList.length>0){
      return ListView.builder(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          itemCount: widgetList.length,
          padding: new EdgeInsets.only(top: 5.0),
          itemBuilder: (context, index) {
            return widgetList[widgetList.keys.toList()[index]];
          });
    }
    else{
      return Container();
    }
  }

  void addWidget(index){
    setState(() {
      TimesInitC[index] = new TextEditingController();
      TimesFinC[index] = new TextEditingController();
      TimesInit[index] = TimeOfDay(hour: 00, minute: 00);
      TimesFin[index] = TimeOfDay(hour: 01, minute: 00);
      TimesInitC[index].text=formatDate(
          DateTime(2019, 08, 1, TimesInit[index].hour, TimesInit[index].minute),
          [hh, ':', nn, " ", am]).toString();
      TimesFinC[index].text=formatDate(
          DateTime(2019, 08, 1, TimesFin[index].hour, TimesFin[index].minute),
          [hh, ':', nn, " ", am]).toString();
      var c = Container(
        padding: new EdgeInsets.only(bottom: 25.0),
        alignment: Alignment.center,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton.icon(
                onPressed: () => {
                  deleteWidget(index)
                },
                color: Colors.blue,
                icon: Icon(Icons.delete,color: Colors.black),
                label: Text(oficios[sub][of])
            ),
            Column(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Text(
                      'Hora de Inicio',
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 0.5),
                    ),
                    InkWell(
                      onTap: () {
                        _selectTime(context,index,TimesInit[index],TimesInitC[index], true);
                      },
                      child: Container(
                        width: 100,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(color: Colors.grey[200]),
                        child: TextFormField(
                          style: TextStyle(fontSize: 15),
                          textAlign: TextAlign.center,
                          enabled: false,
                          keyboardType: TextInputType.text,
                          controller: TimesInitC[index],
                          decoration: InputDecoration(
                              disabledBorder:
                              UnderlineInputBorder(borderSide: BorderSide.none),
                              contentPadding: EdgeInsets.all(5)),
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    Text(
                      'Hora Fin',
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontWeight: FontWeight.w600,
                          letterSpacing: 0.5),
                    ),
                    InkWell(
                      onTap: () {
                        _selectTime(context,index,TimesFin[index],TimesFinC[index], false);
                      },
                      child: Container(
                        width: 100,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(color: Colors.grey[200]),
                        child: TextFormField(
                          style: TextStyle(fontSize: 15),
                          textAlign: TextAlign.center,
                          enabled: false,
                          keyboardType: TextInputType.text,
                          controller: TimesFinC[index],
                          decoration: InputDecoration(
                              disabledBorder:
                              UnderlineInputBorder(borderSide: BorderSide.none),
                              // labelText: 'Time',
                              contentPadding: EdgeInsets.all(5)),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      );
      widgetList[of] = c;
    });
  }

  void deleteWidget(index){
    setState(() {
      widgetList.remove(index);
      TimesInit.remove(index);
      TimesInitC.remove(index);
      TimesFin.remove(index);
      TimesFinC.remove(index);
    });
  }

  Widget botonAumentarWidget(BuildContext context){
    return RaisedButton.icon(
        onPressed: () => {
          if(of!='-1'){
            addWidget(of),
          }
        },
        color: Colors.blue,
        icon: Icon(Icons.add,color: Colors.black),
        label: Text("Agregar")
    );
  }

  Widget dropdown1(){
    return DropdownButton<String>(
        value: padre,
        icon: const Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        style: const TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          setState(() {
            padre = newValue;
            sub=suboficios[padre].keys.toList()[0];
            of=oficios[sub].keys.toList()[0];
          });
        },
        items: padres.keys.toList().map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(padres[value]),
          );
        }).toList(),
    );
  }

  Widget dropdown2(){
    return DropdownButton<String>(
        value: sub,
        icon: const Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        style: const TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          setState(() {
            sub = newValue;
            of=oficios[sub].keys.toList()[0];
          });
        },
        items: suboficios[padre].keys.toList().map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(suboficios[padre][value]),
          );
        }).toList(),
    );
  }
  Widget dropdown3(){
    return DropdownButton<String>(
      value: of,
      icon: const Icon(Icons.arrow_downward),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: Colors.deepPurple),
      underline: Container(
        height: 2,
        color: Colors.deepPurpleAccent,
      ),
      onChanged: (String newValue) {
        setState(() {
          of = newValue;
        });
      },
      items: oficios[sub].keys.toList().map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(oficios[sub][value]),
        );
      }).toList(),
    );
  }
}

